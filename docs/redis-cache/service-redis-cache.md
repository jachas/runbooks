<!-- MARKER: do not edit this section directly. Edit services/service-catalog.yml then run scripts/generate-docs -->
#  Redis-cache Service
* [Service Overview](https://dashboards.gitlab.net/d/redis-cache-main/redis-cache-overview)
* **Alerts**: https://alerts.gitlab.net/#/alerts?filter=%7Btype%3D%22redis-cache%22%2C%20tier%3D%22db%22%7D
* **Label**: gitlab-com/gl-infra/production~"Service:RedisCache"

## Logging

* [system](https://log.gprd.gitlab.net/goto/1a4342231de57c0ceabc8f5e0e402909)

## Troubleshooting Pointers

* [../redis/redis.md](../redis/redis.md)
<!-- END_MARKER -->
