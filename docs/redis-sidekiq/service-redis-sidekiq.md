<!-- MARKER: do not edit this section directly. Edit services/service-catalog.yml then run scripts/generate-docs -->
#  Redis-sidekiq Service
* [Service Overview](https://dashboards.gitlab.net/d/redis-sidekiq-main/redis-sidekiq-overview)
* **Alerts**: https://alerts.gitlab.net/#/alerts?filter=%7Btype%3D%22redis-sidekiq%22%2C%20tier%3D%22db%22%7D
* **Label**: gitlab-com/gl-infra/production~"Service:RedisSidekiq"

## Logging

* [system](https://log.gprd.gitlab.net/goto/80a1ff609f91b0fb2b770a3a70784be0)

## Troubleshooting Pointers

* [../redis/redis.md](../redis/redis.md)
* [../sidekiq/sidekiq-survival-guide-for-sres.md](../sidekiq/sidekiq-survival-guide-for-sres.md)
<!-- END_MARKER -->
