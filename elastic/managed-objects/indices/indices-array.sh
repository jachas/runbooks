#!/bin/bash

declare -a indices

export indices=(
  camoproxy
  consul
  gitaly
  gke
  mailroom
  monitoring
  pages
  postgres
  praefect
  pubsubbeat
  puma
  rails
  redis
  registry
  runner
  shell
  sidekiq
  system
  workhorse
)
